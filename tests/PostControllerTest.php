<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Process\Process;


class PostControllerTest extends WebTestCase
{
    public function testShowAllArticles()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Blog');
        
        $this->assertSelectorExists('.post');

        $this->assertEquals('16', $crawler->filter('.post')->count());
        $this->assertSelectorTextContains('.post', 'Title 1');

        
    }
}