<?php

namespace App\DataFixtures;

use App\Entity\Post;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{   
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder) {
        $this->encoder = $encoder;
    }
    
    public function load(ObjectManager $manager)
    {
       for ($i=1; $i < 5; $i++) { 

            $user = new User();
            $hashedPassword = $this->encoder->encodePassword($user, '1234');

            $user->setEmail('mail'.$i.'@mail.com')
            ->setPassword($hashedPassword)
            ->setRoles(['ROLE_USER']);

            for ($y=1; $y < 5; $y++) { 
                $post = new Post();
                $post->setTitle("Title ".$y)
                ->setContent("The content of the post number ".$y)
                ->setAuthor($user);
                $manager->persist($post);
            }
            $manager->persist($user);
        }

        $user = new User();
        $hashedPassword = $this->encoder->encodePassword($user, 'admin');

        $user->setEmail('admin@admin.com')
        ->setPassword($hashedPassword)
        ->setRoles(['ROLE_ADMIN']);
        $manager->persist($user);


        $manager->flush();
    }

}